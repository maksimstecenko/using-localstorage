import {
  appInputFormJoinOurProgram,
  appInputFormInputEmailJoinOurProgram,
  createSectionJoinOurProgram,
} from './join-us-section.js';

import { validate } from './email-validator.js';// eslint-disable-next-line
                     
/* const htmlBody = document.getElementById("app-container") */

document.addEventListener('DOMContentLoaded', createSectionJoinOurProgram());

appInputFormJoinOurProgram.addEventListener('submit', (event) => {
  event.preventDefault();
  const emailValue = appInputFormInputEmailJoinOurProgram.value;

  console.log(emailValue);

  console.log(`Entered email: ${emailValue}`);
});

const emailInput = document.querySelector('.app-form-input-email_join-our-program');
const submitButton = document.querySelector('.app-form-input-submit_join-our-program');

/*submitButton.addEventListener('click', () => {
  const email = emailInput.value;
  if (validate(email)) {
    alert('Email is valid');
  } else {
    alert('Wrong email');
  }
});*/

emailInput.addEventListener('input', (event) => {
  const email = event.target.value;
  localStorage.setItem('subscriptionEmail', email);
});

window.addEventListener('load', () => {
  const email = localStorage.getItem('subscriptionEmail');
  if (email) {
    emailInput.value = email;
  }
});

submitButton.addEventListener('click', (event) => {
  event.preventDefault();
  const email = emailInput.value;
  const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
  if (emailRegex.test(email)) {
    emailInput.style.display = 'none';
    submitButton.style.textAlign = 'center';
    submitButton.textContent = 'Unsubscribe';
    localStorage.setItem('subscribed', true);
  }
  else if (localStorage.getItem('subscribed')) {
    emailInput.style.display = 'block';
    emailInput.value = '';
    submitButton.style.textAlign = 'left';
    submitButton.textContent = 'Subscribe';
    localStorage.removeItem('subscribed');
    localStorage.removeItem('subscriptionEmail');
  }
});

